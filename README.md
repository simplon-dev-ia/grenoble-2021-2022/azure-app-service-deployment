# Azure App Service Deployment

> Learning how to deploy a Python Flask app using Azure App Service PaaS

## Objectives

- Get a sense of what Azure App Service can do
- Deploy a web application on Azure App Service

## Information Research

- [ ] For which use-case(s) would I need to use Azure App Service?
- [ ] What programming languages are supported by Azure App Service?
- [ ] How can I deploy an application written with an unsupported language on Azure App Service?
- [ ] How do I secure a server with Azure App Service?

## Azure App Service

For ease of deployment, we will use Azure App Service for Linux which allows
developers to deploy web applications with a PaaS experience, similar to Heroku.

Supported Python web frameworks only require a few files:

- `app.py`: a Python WSGI application
- `requirements.txt`: Python dependencies

For this activity, these files are provided:

- `app.py`: simple Flask application
- `requirements.txt`: Python dependencies, generated using `pipenv lock -r > requirements.txt`

For other non-supported frameworks such as FastAPI, you can tweak some parameters
to customize the application startup command. Or we can use a Docker image.

Documentation:

- [Quickstart Python apps](https://docs.microsoft.com/en-us/azure/app-service/quickstart-python)
- [Configure Python apps](https://docs.microsoft.com/en-us/azure/app-service/configure-language-python)
- [Deploy with Git](https://docs.microsoft.com/en-us/azure/app-service/deploy-local-git)
- [Quickstart Docker apps](https://docs.microsoft.com/en-us/azure/app-service/quickstart-custom-container)

## Setup

For security reasons, the following Azure resources are automatically provisionned
for you:

- A common resource group containing all the resources for the activity: `activite-app-service`
- A common App Service plan containing all apps: `activite-app-service-asp`
- A personal App Service instance for each student: `activite-app-service-<username>`
- A role of level "Contributor" for each personal App Service instance

**Thus, you only have access to your own personal App Service instance!**

## Workflow

As we are using the deployment mode "LocalGit", we can deploy an app to App Service
with a simple "git push" worklow. 

Before we can deploy the app, we need to retrieve the Git URL and the deployment credentials:

- [ ] Go to the Azure portal: https://portal.azure.com
- [ ] Select the "All Resources" section
- [ ] Select your App Service instance named "activite-app-service-username"
- [ ] Take note of the **application URL**
- [ ] In "Deployment", select "Deployment Center" then "Local Git/FTPS credentials"
- [ ] In "Application Scope" section take note of the following:
    - **Git clone Uri**
    - **Username**: only after the `\` character and starting with the `$` character
    - **Password**

Follow these steps to deploy the Flask application:

- [ ] Clone this Git repository:
- [ ] Add the Git remote for your Azure App Service instance using the **Git clone Uri**:
```bash
git remote add azure https://your-instance-name.scm.azurewebsites.net/your-instance-name.git
```
- [ ] Deploy to Azure App Service:
```bash
git push azure main:master
```
- [ ] Enter the **username** and **password** for your App Service instance when asked
- [ ] Open the **application URL** in your browser: https://your-instance-name.azurewebsites.net

**Be patient** when trying to open the application in the browser, it can take a few
minutes for the initial startup.

## Exercice

- [ ] Modify the Flask application in the file `app.py`
- [ ] Commit the changes to Git
- [ ] Re-deploy the new application on Azure App Service
